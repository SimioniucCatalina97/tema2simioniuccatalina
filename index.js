
const FIRST_NAME = "Beatrice";
const LAST_NAME = "Simioniuc";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
    let cache ={};
    cache.home = 0;

cache.about =0;

cache.contact=0;

pageAccessCounter = (section = ' ') => {
    if((section ==='home')||(section ==='HOME')||(section === ' ')){

        cache.home =cache.home + 1 ;
    }
    if((section ==='about')||(section ==='ABOUT')){
        cache.about=cache.about + 1 ;
    }

    if((section ==='contact')||(section ==='CONTACT')){
        cache.contact= cache.contact + 1;
    }
}

getCache = () =>{
    return cache;
}


return {
    pageAccessCounter : pageAccessCounter,
    getCache :getCache
}


}
   


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

